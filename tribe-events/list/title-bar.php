<?php
/**
 * List View Title Template
 * The title template for the list view of events.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/title-bar.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 * @since   4.6.19
 *
 */
?>

<div class="events-title-area">

	<!-- List Title -->
	<?php do_action( 'tribe_events_before_the_title' ); ?>
	<p class="">Holy Family hosts several annual fundraising events to support its life-changing programs and services. In addition to individual tickets, corporate sponsor opportunities are also available for each signature event. Even if you are unable to attend, items for the silent auction and prizes and always needed and appreciated.</p>
	<p>For more information about any of these events, please contact Carina Martin at 412.766.9020 ext. 1207 or <a href="sendto:Martin.Carina@hfi-pgh.org">Martin.Carina@hfi-pgh.org</a>.</p>
	<hr>
	<h3>Explore our Events</h3>
	
	<?php do_action( 'tribe_events_after_the_title' ); ?>

</div>
