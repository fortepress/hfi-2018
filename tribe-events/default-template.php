<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Display -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.23
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header();
?>

<?php
$container   = get_theme_mod( 'understrap_container_type' );
?>
<div class="wrapper blog-home" id="full-width-page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">
			
				<main class="site-main" id="main" role="main">
					<div class="full-width-header">
						<?php //echo wp_get_attachment_image( '41', $size ); ?>
						<?php  hfi_hero_image( '127' ); ?>
						<header class="entry-header">

						<h1 class="entry-title">What You Can Do</h1>

						</header><!-- .entry-header -->
					</div><!-- .full-width-header -->

					<div class="row">
						<div id="hfi-mobile-tabs" class="internal-left col-md-3">
							
								<div class="left-title">
									<h3>What You Can Do</h3>
								</div>
								<?php 
								wp_nav_menu(
									array(
										'theme_location'  => 'what_you_can_do',
										'container_class' => 'internal-menu',
										'container_id'    => 'internal-menu',
										'menu_class'      => '',
										'fallback_cb'     => '',
										'menu_id'         => '',
										'depth'           => 2,
										'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
									)
								); 
								?>
							
						</div><!-- .col-md-3 -->
	
						<div class="col-md-9">
							<div class="entry-content">
								<h3>Support an Event</h3>
								

									<main id="tribe-events-pg-template" class="tribe-events-pg-template">
										<?php tribe_events_before_html(); ?>
										<?php tribe_get_view(); ?>
										<?php tribe_events_after_html(); ?>
									</main> <!-- #tribe-events-pg-template -->
									</div><!-- .entry-content -->
						</div><!-- .col-md-9 -->
						
					</div><!-- .row -->

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->
<?php
get_footer();
