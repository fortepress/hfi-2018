<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper blog-home" id="full-width-page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

<div class="row">

	<div class="col-md-12 content-area" id="primary">
	
		<main class="site-main" id="main" role="main">
			<div class="full-width-header">
				<?php //echo wp_get_attachment_image( '41', $size ); ?>
				<?php  hfi_hero_image( '41' ); ?>
				<header class="entry-header">

				<h1 class="entry-title">Who We Are</h1>

				</header><!-- .entry-header -->
			</div><!-- .full-width-header -->

			<div class="row">
				<div id="hfi-mobile-tabs" class="internal-left col-md-3">
					
						<div class="left-title">
							<h3>Who We Are</h3>
						</div>
						<?php 
						wp_nav_menu(
							array(
								'theme_location'  => 'who_we_are',
								'container_class' => 'internal-menu',
								'container_id'    => 'internal-menu',
								'menu_class'      => '',
								'fallback_cb'     => '',
								'menu_id'         => '',
								'depth'           => 2,
								'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
							)
						); 
						?>
					
				</div><!-- .col-md-3 -->

				<div class="col-md-9">
					<div class="entry-content">
						<h3><?php the_title(); ?></h3>
						<hr>
						
					<?php if ( have_posts() ) : ?>


						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'loop-templates/content', 'single' ); ?>

		<?php understrap_post_nav(); ?>

	<?php
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
	?>

<?php endwhile; // end of the loop. ?>

						

				

						<?php endif; ?>

					</div><!-- .entry-content -->
				</div><!-- .col-md-9 -->
				
			</div><!-- .row -->

		</main><!-- #main -->

	</div><!-- #primary -->

</div><!-- .row end -->

</div><!-- Container end -->

</div><!-- Wrapper end -->
<?php $publications = new HFI_Publications();
$publications->the_publications();

get_footer();?>
