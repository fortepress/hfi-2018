<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>
<?php do_action('hfi_footer_top'); ?>
<?php get_template_part( 'sidebar-templates/sidebar', 'footertop' ); ?>
<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="container-fluid<?php //echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info">

						<?php understrap_site_info(); ?>

					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->
<script>
	jQuery(document).ready(function($){
		$(window).scroll(function (event) {
			var scroll = $(window).scrollTop();
			if( scroll > 10 ) {
				$('#wrapper-navbar').addClass('scrolled');
				$('.custom-logo-link img').attr('src', '<?php echo get_stylesheet_directory_uri(); ?>/img/logo-color.svg');
			} else {
				$('#wrapper-navbar').removeClass('scrolled');
				$('.custom-logo-link img').attr('src', '<?php echo get_stylesheet_directory_uri(); ?>/img/logo.svg');
			}
		});
	});
</script>
<?php wp_footer(); ?>

</body>

</html>

