<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<nav class="navbar navbar-expand-md fixed-top">

		<?php if ( 'container' == $container ) : ?>
			<div class="container" >
		<?php endif; ?>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } else {
						the_custom_logo();
					} ?><!-- end custom logo -->

				<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button> -->

				<!-- The WordPress Menu goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						// 'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>

				<div class="menu-right-wrapper">
					<?php hfi_donate_button(); ?>
			
					<button class="hamburger-menu" data-toggle="modal" data-target="#side-nav"><i class="fa fa-bars"></i></button>
				</div>
				
				<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>
			
		</nav><!-- .site-navigation -->
		<div class="modal" id="side-nav" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="dropdown-header">
							<?php hfi_donate_button(); ?>
			
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true"><img src="<?php echo get_template_directory_uri(); ?>/img/close-icon.svg" ></span>
							</button>
						</div><!-- .dropdown-header -->
						<?php wp_nav_menu(
							array(
								'theme_location'  => 'primary',
								'container_class' => 'dropdown-top-menu',
								'container_id'    => 'dropdown-top-menu',
								'menu_class'      => '',
								'fallback_cb'     => '',
								'menu_id'         => '',
								'depth'           => 2,
								'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
							)
						); ?>
					</div><!-- .modal-header -->
					<div class="modal-body">
						<?php wp_nav_menu(
							array(
								'theme_location'  => 'dropdown_bottom',
								'container_class' => 'dropdown-top-menu',
								'container_id'    => 'dropdown-top-menu',
								'menu_class'      => '',
								'fallback_cb'     => '',
								'menu_id'         => '',
								'depth'           => 2,
								'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
							)
						); ?>
						<div class="social-and-search">
							<?php hfi_social_icons( 'dark' ); ?>

							<?php wp_nav_menu(
								array(
									'theme_location'  => 'search',
									'container_class' => 'search-menu',
									'container_id'    => 'search-menu',
									'menu_class'      => '',
									'fallback_cb'     => '',
									'menu_id'         => '',
									'depth'           => 2,
									'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
								)
							); ?>
						</div>
						
					</div><!-- .modal-body -->
				</div>
				
			
			</div>
		</div>
	</div><!-- #wrapper-navbar end -->
