<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_filter( 'body_class', 'understrap_body_classes' );

if ( ! function_exists( 'understrap_body_classes' ) ) {
	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 *
	 * @return array
	 */
	function understrap_body_classes( $classes ) {
		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		return $classes;
	}
}

// Removes tag class from the body_class array to avoid Bootstrap markup styling issues.
add_filter( 'body_class', 'understrap_adjust_body_class' );

if ( ! function_exists( 'understrap_adjust_body_class' ) ) {
	/**
	 * Setup body classes.
	 *
	 * @param string $classes CSS classes.
	 *
	 * @return mixed
	 */
	function understrap_adjust_body_class( $classes ) {

		foreach ( $classes as $key => $value ) {
			if ( 'tag' == $value ) {
				unset( $classes[ $key ] );
			}
		}

		return $classes;

	}
}

// Filter custom logo with correct classes.
add_filter( 'get_custom_logo', 'understrap_change_logo_class' );

if ( ! function_exists( 'understrap_change_logo_class' ) ) {
	/**
	 * Replaces logo CSS class.
	 *
	 * @param string $html Markup.
	 *
	 * @return mixed
	 */
	function understrap_change_logo_class( $html ) {

		$html = str_replace( 'class="custom-logo"', 'class="img-fluid"', $html );
		$html = str_replace( 'class="custom-logo-link"', 'class="navbar-brand custom-logo-link"', $html );
		$html = str_replace( 'alt=""', 'title="Home" alt="logo"' , $html );

		return $html;
	}
}

/**
 * Display navigation to next/previous post when applicable.
 */

if ( ! function_exists ( 'understrap_post_nav' ) ) {
	function understrap_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
				<nav class="container navigation post-navigation">
					<h2 class="sr-only"><?php _e( 'Post navigation', 'understrap' ); ?></h2>
					<div class="row nav-links justify-content-between">
						<?php

							if ( get_previous_post_link() ) {
								previous_post_link( '<span class="nav-previous">%link</span>', _x( '<i class="fa fa-angle-left"></i>&nbsp;%title', 'Previous post link', 'understrap' ) );
							}
							if ( get_next_post_link() ) {
								next_post_link( '<span class="nav-next">%link</span>',     _x( '%title&nbsp;<i class="fa fa-angle-right"></i>', 'Next post link', 'understrap' ) );
							}
						?>
					</div><!-- .nav-links -->
				</nav><!-- .navigation -->

		<?php
	}
}

/*
Add support for SVGs
*/
function understrap_add_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
}
add_action('upload_mimes', 'understrap_add_file_types_to_uploads');

function hfi_donate_button() {
	echo '<a class="donate-btn" href="'. get_site_url() .'/donate">Donate</a>';
}

function hfi_blog_feed() {
	$args = array(
		'posts_per_page' => 4,
		'post_type' => 'post',

	);
	$blog_feed = get_posts( $args );
	$html = '<ul class="front-page-blog-feed">';
	foreach( $blog_feed as $blog ) {
		$html .= '<li>';
			$html .= '<a class="blog-title" href="'. get_permalink( $blog->ID ).'">'. $blog->post_title . '</a>';
			$html .= '<span class="blog-date">' . get_the_date( 'F j, Y' , $blog->ID ) . '</span>';
		$html .= '</li>';
	}
	$html .= '</ul>';
	$html .= '<a class="view-all" href="' . get_site_url() . '/news">View All</a>';
	echo $html;
}

function hfi_social_icons($color = 'light' ){
	$facebook = get_field( 'facebook', 'option' );
	$twitter = get_field( 'twitter', 'option' );
	$instagram = get_field( 'instagram', 'option' );
	$linkedin = get_field( 'linkedin', 'option' );
	$style = 'dark' === $color ? '-blue' : '';
	echo '<div class="social-icons" >';
		if ( $facebook ) {
			echo '<a href="'. $facebook .'"><img src="' . get_template_directory_uri() .  '/img/facebook'. $style . '.svg" /></a>';
		}
		if ( $twitter ) {
			echo '<a href="'. $twitter .'"><img src="' . get_template_directory_uri() .  '/img/twitter'. $style . '.svg" /></a>';
		}
		if ( $instagram ) {
			echo '<a href="'. $instagram .'"><img src="' . get_template_directory_uri() .  '/img/instagram'. $style . '.svg" /></a>';
		}
		if( $linkedin ) {
			echo '<a href="'. $linkedin .'"><img src="' . get_template_directory_uri() .  '/img/linkedin'. $style . '.svg" /></a>';
		}	
	echo '</div>';
}

function hfi_internal_menu( $menu_title ){
	wp_nav_menu(
		array(
			'theme_location'  => $menu_title,
			'container_class' => 'internal-menu',
			'container_id'    => 'internal-menu',
			'menu_class'      => '',
			'fallback_cb'     => '',
			'menu_id'         => '',
			'depth'           => 2,
			'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
		)
	); 
}

function hfi_boards_of_directors(){
	$tabs = hfi_get_directors();
	//var_dump($tabs);
	$boards = new HF_Tabbed_Content( $tabs );
	echo '<div id="hfi-boards-tabs">';
	$boards->the_tabs();
	$boards->the_tab_contents();
	echo '</div>';
	
}

function hfi_call_to_action() {
	if ( ! is_page( 'join-our-mailing-list' ) ) {
		echo do_shortcode('[fl_builder_insert_layout id="7295"]');
	}
	
}

function hfi_get_directors() {
	$boards = get_posts( array( 'post_type' => 'hfi-boards', 'order' => 'ASC' ) );
	$tabs = array();
	foreach( $boards as $board ){
		$tab = array();
		$tab['section_title'] = $board->post_title;
		$tab['section_id'] = $board->ID;
		$tab['section_content'] = hfi_directors( $board->ID );
		$tabs[] = $tab;
	}
	return $tabs;
}
function hfi_directors( $id ) {
	$directors = get_field('directors', $id );
	return $directors;
}