<?php

class HF_Tabbed_Content {

	public $tab_contents = '';

	public $tabs = '';

	public $tab_content = array();

	function __construct( $tab_content = array() ) {
		$this->tab_content = $tab_content;
		$this->build_tabs();
	}

	function build_tabs(){
		if ( ! empty( $this->tab_content ) ){
			//var_dump( $this->tab_content);
			foreach( $this->tab_content as $tab ) {
				$this->tabs .= $this->build_tab_section( $tab[ 'section_title' ], $tab[ 'section_id' ] );
				$this->tab_contents .= $this->build_content_section( $tab[ 'section_title' ], $tab[ 'section_id' ], $tab[ 'section_content' ] );
			}
		}	
	}

	function build_tab_section( $section_title, $section_id ) {
		
			$html = '<li class="">';
				$html .= '<a href="#' . $section_id . '">' .$section_title .' </a>';
			$html .= '</li>';

		return $html;
	}

	function build_content_section( $section_title, $section_id, $people ) {
		$html = '<div class="board" id="'.$section_id.'">';
			foreach( $people as $person ){
				$html .= '<div class="person">';
					$html .= '<p>' .$person['first_name'] . ' ' . $person['last_name'] . '</p>';
					$html .= '<p>' . $person['title'] . '</p>';
					$html .= '<p>' . $person['company'] . '</p>';
				$html .= '</div>';
			}
		$html .= '</div>';
		return $html;
	}

	function the_tabs(){
		echo '<ul class="nav nav-tabs nav-tabs--vertical nav-tabs--left" role="navigation">';
			echo $this->tabs;
			//echo '<div class="indicator"></div>';
		echo '</ul>';

	}

	function the_tab_contents(){
		echo '<div>';
			echo $this->tab_contents;
		echo '</div>';

	}
}