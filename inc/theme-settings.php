<?php
/**
 * Check and setup theme's default settings
 *
 * @package understrap
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists ( 'understrap_setup_theme_default_settings' ) ) {
	function understrap_setup_theme_default_settings() {

		// check if settings are set, if not set defaults.
		// Caution: DO NOT check existence using === always check with == .
		// Latest blog posts style.
		$understrap_posts_index_style = get_theme_mod( 'understrap_posts_index_style' );
		if ( '' == $understrap_posts_index_style ) {
			set_theme_mod( 'understrap_posts_index_style', 'default' );
		}

		// Sidebar position.
		$understrap_sidebar_position = get_theme_mod( 'understrap_sidebar_position' );
		if ( '' == $understrap_sidebar_position ) {
			set_theme_mod( 'understrap_sidebar_position', 'right' );
		}

		// Container width.
		$understrap_container_type = get_theme_mod( 'understrap_container_type' );
		if ( '' == $understrap_container_type ) {
			set_theme_mod( 'understrap_container_type', 'container' );
		}
	}
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Home Page Settings',
		'menu_title'	=> 'Home Page',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}

add_image_size( 'internal-featured', 1335, 433, true );
add_image_size( 'hero-mobile', 576, 786, true );
add_image_size( 'event-preview', 280, 188, true );

function hfi_hero_image( $attachment_id ) {
	$src_big = wp_get_attachment_image_src( $attachment_id, 'full' );
	//var_dump($scr_big);
	$src_sm = wp_get_attachment_image_src( $attachment_id, 'hero-mobile' );
	?>
	<img width="1920" height="726" 
	src="<?php echo $src_big[0]; ?>" 
	class="attachment- size-" alt="" 
	srcset="<?php echo $src_big[0]; ?> 1920w, 
	<?php echo $src_sm[0]; ?> 576w, " sizes="(max-width: 576px) 33vw,  (max-width: 1920px) 100vw, 1920px">
	<?php
}
