<?php
/**
 * Sidebar setup for footer top.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );

?>

<?php if ( is_active_sidebar( 'footertop' ) ) : ?>

	<!-- ******************* The Footer Full-width Widget Area ******************* -->

	<div class="wrapper wrapper-footer-top" id="wrapper-footer-top">

		<div class="<?php echo esc_attr( $container ); ?> footer-top-content" id="footer-top-content" tabindex="-1">

			<div class="row footer-top-row">

				<?php dynamic_sidebar( 'footertop' ); ?>

			</div>

		</div>

	</div><!-- #wrapper-footer-top -->

<?php endif; ?>
