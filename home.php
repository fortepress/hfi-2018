<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_header();
?>

<?php
$container   = get_theme_mod( 'understrap_container_type' );
?>
<div class="wrapper blog-home" id="full-width-page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">
			
				<main class="site-main" id="main" role="main">
					<div class="full-width-header">
						<?php //echo wp_get_attachment_image( '41', $size ); ?>
						<?php  hfi_hero_image('41' ); ?>
						<header class="entry-header">

						<h1 class="entry-title">Who We Are</h1>

						</header><!-- .entry-header -->
					</div><!-- .full-width-header -->

					<div class="row">
						<div id="hfi-mobile-tabs" class="internal-left col-md-3">
							
								<div class="left-title">
									<h3>Who We Are</h3>
								</div>
								<?php 
								wp_nav_menu(
									array(
										'theme_location'  => 'who_we_are',
										'container_class' => 'internal-menu',
										'container_id'    => 'internal-menu',
										'menu_class'      => '',
										'fallback_cb'     => '',
										'menu_id'         => '',
										'depth'           => 2,
										'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
									)
								); 
								?>
							
						</div><!-- .col-md-3 -->
	
						<div class="col-md-9">
							<div class="entry-content">
								<h3>News</h3>
								<p class="news-subheading">Stories You Don’t Want to Miss</p>
								<hr>
								
							<?php if ( have_posts() ) : ?>


								<?php /* Start the Loop */ ?>
								<?php while ( have_posts() ) : the_post(); ?>

									<?php
									/*
									* Include the Post-Format-specific template for the content.
									* If you want to override this in a child theme, then include a file
									* called content-___.php (where ___ is the Post Format name) and that will be used instead.
									*/
									get_template_part( 'loop-templates/content', get_post_format() );
									?>

								<?php endwhile; ?>

								<!-- The pagination component -->
								<?php understrap_pagination(); ?>

								<?php else : ?>

								<?php get_template_part( 'loop-templates/content', 'none' ); ?>

								<?php endif; ?>

							</div><!-- .entry-content -->
						</div><!-- .col-md-9 -->
						
					</div><!-- .row -->

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->
<?php $publications = new HFI_Publications();
$publications->the_publications();
 get_footer(); ?>