<?php
/**
 * The template for displaying search results pages.
 *
 * @package understrap
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">

				<main class="site-main" id="main" role="main">
					<div class="full-width-header">
						<?php //echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
						<?php hfi_hero_image( 41 ); ?>
						<header class="entry-header">

							<h1 class="entry-title">Search Results</h1>

						</header><!-- .entry-header -->
					</div><!-- .full-width-header -->

					<div class="row">
						<div class="internal-left col-md-3">
						
							<div class="internal-left-content">
								<p class="search-text">Search Results for: </p>
								<p class="search-result">"<?php echo get_search_query();  ?>"</p>
								<div><?php get_search_form(true); ?></div>
							</div>					
						</div><!-- .col-md-3 -->

						<div class="col-md-9" id="primary">
							<div class="entry-content" >
								<?php if ( have_posts() ) : ?>
									<?php while ( have_posts() ) : the_post(); ?>

										<?php
										/**
										 * Run the loop for the search to output the results.
										 * If you want to overload this in a child theme then include a file
										 * called content-search.php and that will be used instead.
										 */
										get_template_part( 'loop-templates/content', 'search' );
										?>

							
									<?php endwhile; // end of the loop. ?>
									<?php else : ?>

							<?php get_template_part( 'loop-templates/content', 'none' ); ?>

						<?php endif; ?>

									<!-- The pagination component -->
									<?php understrap_pagination(); ?>
							</div>
						</div><!-- #primary -->

					</div><!-- .row -->

				</main><!-- .site-main -->

			</div><!-- #primary -->
		</div><!-- .row -->
	</div><!-- .content -->
</div><!-- .wrapper -->


<?php get_footer(); ?>



	

			