<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
?>
<?php get_template_part( 'sidebar-templates/sidebar', 'statichero' ); ?>

<div id="front-page-main" class="row">
	<div class="col-md-6 what-you-can-do">
		<div class="what-you-can-do-text">
			<h3><?php the_field( 'secondary_sub_heading', 'option' ); ?></h3> 
			<h2><?php the_field( 'secondary_heading', 'option' ); ?></h2>
			<p><?php the_field( 'secondary_text', 'option' ); ?></p>
			<a class="hf-button" href="<?php the_field( 'secondary_button_link', 'option' ); ?>"><?php the_field( 'secondary_button_text', 'option' ); ?></a>
		</div>
	</div>
	<div class="col-md-6 blog-list">
		<h3>News Feed</h3>
		<?php hfi_blog_feed(); ?> 
	</div>
</div>


<?php

get_footer();