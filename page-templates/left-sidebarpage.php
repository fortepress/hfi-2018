<?php
/**
 * Template Name: Left Sidebar Layout
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">

				<main class="site-main" id="main" role="main">
					<div class="full-width-header">
						<?php //echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
						<?php hfi_hero_image( get_post_thumbnail_id( $post->ID ) ); ?>
						<header class="entry-header">

							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

						</header><!-- .entry-header -->
					</div><!-- .full-width-header -->

					<div class="row">
						<div class="internal-left main-nav-page col-md-3">
							<?php the_field('left_sidebar_content'); ?>
						</div><!-- .col-md-3 -->

						<div class="col-md-9" id="primary">
							<?php while ( have_posts() ) : the_post(); ?>

								<?php the_content(); ?>

					
							<?php endwhile; // end of the loop. ?>


						</div><!-- #primary -->

					</div><!-- .row -->

				</main><!-- .site-main -->

			</div><!-- #primary -->
		</div><!-- .row -->
	</div><!-- .content -->
</div><!-- .wrapper -->


<?php get_footer(); ?>
