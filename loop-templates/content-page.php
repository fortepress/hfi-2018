<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
$page_type = get_field( 'page_type' );
$hero_id = get_field( $page_type['value'] . '_image', 'options' );
//var_dump($hero_id);
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	
	<div class="full-width-header">
		<?php //echo wp_get_attachment_image( $hero_id, $size ); ?>
		<?php hfi_hero_image( $hero_id ); ?>
		
		<header class="entry-header">

			<h1 class="entry-title"><?php echo $page_type['label']; ?></h1>

		</header><!-- .entry-header -->
	</div>
	<div class="row">
		<div id="hfi-mobile-tabs" class="internal-left col-md-3">
			<div class="left-title">
				<h3><?php echo $page_type['label']; ?></h3>
			</div>
			<?php 
			wp_nav_menu(
				array(
					'theme_location'  => $page_type['value'],
					'container_class' => 'internal-menu',
					'container_id'    => 'internal-menu',
					'menu_class'      => '',
					'fallback_cb'     => '',
					'menu_id'         => '',
					'depth'           => 2,
					'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
				)
			); 
			?>
			<?php //$tabbed_content->the_tabs(); ?>
				
			<?php //$tabbed_content->the_tab_contents(); ?>
			
		</div>
	
		<div class="col-md-9">
			<div class="entry-content">
				<?php the_title('<h3 class="page-title">', '</h3>'); ?>
				<?php the_content(); ?>
			</div>
			<?php the_post_thumbnail('internal-featured'); ?>
		</div><!-- .entry-content -->
		
	</div><!-- .row -->

</article><!-- #post-## -->
